$(document).ready(function () {
    async function updateRegistrationAddress() {
        try {
            return await $.ajax({
                method: 'patch',
                async: true,
                url: `http://localhost:3001/bff/reservations`,
            });
        } catch (err) {
            console.error(err);
        }
    }
});