const axios = require('axios').default;

exports.edit = async (req, res, next) => {
    const {addressLine1, postalCode, city, countryCode} = req.body;
    let url = `https://7syvuumsg3.execute-api.eu-central-1.amazonaws.com/api/reservations/${req.params.id}`;
    let data = {
        billingAddress: {
            addressLine1: addressLine1,
            postalCode: postalCode,
            city: city,
            countryCode: countryCode
        }
    };

    return await axios({
        type: 'PATCH',
        data: data,
        url: url,
        headers: {
            'accept': 'application/json',
            'Authorization': 'Bearer eyJraWQiOiJBTFwvOCtmaVwvSWdrem94ZENhSzlYOVc5K3Z3eFpqK3lsYm50a242Y0E4aG89IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiI2a3A5MG9jN25rOHV0aHVvMjN1YmpwMWsybyIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoibnVtYS1jb3JlLWFwaVwvYWRtaW4iLCJhdXRoX3RpbWUiOjE2NjYyNjM1MTEsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC5ldS1jZW50cmFsLTEuYW1hem9uYXdzLmNvbVwvZXUtY2VudHJhbC0xX09UNVNwNVlzbSIsImV4cCI6MTY2NjM0OTkxMSwiaWF0IjoxNjY2MjYzNTExLCJ2ZXJzaW9uIjoyLCJqdGkiOiIzM2RhMGQzYi00YTI4LTQyOGQtYTZjZC1hNDkxOWY4ZGFjYjciLCJjbGllbnRfaWQiOiI2a3A5MG9jN25rOHV0aHVvMjN1YmpwMWsybyJ9.YxNqTnUHYsXWjGXbbGqZfAzcRSqgvQqmrb3L8t-gVz9OGT1D3_5J88pkpWjP0Dwen88ZrzTuxY6bw7LHWXzbCFWzOAO1PBALr1oCCq4vUoK_MmJninZKK_DFp1gsfmTmoUwg8i2nPVYpRZYdxOv6Eog6YQ_UyL34bm_45qlguf23uoRaTfrxcR_9dw4dhmau6R4U808_4L_dlJ1HnkuKRdlihyEqc1t6glqt-ykxVMHSodEet9W1vj8jpEOYZUaD0FHGFm1hlmYSI70L1EqmowuhXUOGk3NjvDFgwdqD3HjxGZBjJ3G7zi3q7iDErYYCxON3CYKCfMhMeVjH2jN3OQ'
        }
    }).then(data => {
        console.log('Reservation successfully updated');
        console.log(data.data);
        next();
    }).catch(function (error) {
        console.log(`Reservation update failed: ${error.message}`);
        next(error);
    });
};

exports.getById = async (req, res, next) => {

};