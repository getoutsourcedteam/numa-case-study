const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const config = require("./config/config");
const cors = require('cors')

//app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/", (req, res) => {
  res.send("Welcome Numa BFF");
});

app.use("/bff/reservations", require("./routes/reservations.routes"));

app.listen(
  config.get("app").port,
  console.log(`Server started on port ${config.get("app").port}...`)
);
