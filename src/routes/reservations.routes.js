const express = require('express');
const router = express.Router();
const reservationsController = require('../controllers/reservations.controller');

// PATCH /bff/reservations/:id
router.patch('/:id', reservationsController.edit);

// GET /bff/reservations/:id
router.get('/:id', reservationsController.getById);

module.exports = router;