require('dotenv').config();
const convict = require('convict');

const config = convict({
    env: {
        doc: "The application environment.",
        format: ["prod", "dev", "test", "stage"],
        default: "dev",
        env: "NODE_ENV"
    },
    app: {
        host: {
            doc: "Application host name/IP",
            format: String,
            default: 'localhost',
            env: "APP_HOST"
        },
        port: {
            doc: "Application Port",
            format: 'port',
            default: 3001,
            env: "PORT"
        }
    }
});


config.validate({ allowed: 'strict' }); // throws error if config does not conform to schema

module.exports = config;