**About the project**

Current case study divided to 2 parts:
1. Backend or BFF - it interacts with API
2. Front end application - overall 1 HTML file with some Javascript beautifiers

*There are several assumptions*
1. The authorization mechanism is not implemented - use Bearer token which can be obtained by authentication via API Swagger Docs
2. Reservation ID is HARDCODED (**AAABQEA-1**)

---

## Run the BFF (backend)

1. Check your the Node.js installation by running in the terminal 
   ```node -v``` and ```npm -v```
2. Make sure you've retrieved the current version e.g
   ```v10.15.3```
3. Unzip the attached **ZIP** file to your local machine
4. Go to the root directory of the project
5. Run ```npm install``` in order to install all necessary packages
6. Run the backend server
   ```npm start```
7. Make sure you can see the server started prompt
   ```Server started on port 3001...```

---

## Access Front End

In order to have an access to the front end application 
just go to ```fe``` directory in the new cloned project and open **edit-reservation.html?id=AAABQEA-1**

---